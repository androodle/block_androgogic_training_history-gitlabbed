<?php

/** 
 * Androgogic Training History Block: Delete object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Delete one of the activities
 *
 **/

$id = required_param('id', PARAM_INT);
$DB->delete_records('androgogic_activities',array('id'=>$id));
echo $OUTPUT->notification(get_string('itemdeleted','block_androgogic_training_history'), 'notifysuccess');

?>
