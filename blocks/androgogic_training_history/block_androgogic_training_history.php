<?php
/** 
 * Androgogic Training History Block: Class
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

class block_androgogic_training_history extends block_base {

	function init(){
		$this->title = get_string('plugintitle','block_androgogic_training_history');
		$this->cron = 1;
	} //end function init

	function get_content(){
		global $CFG;		if ($this->content !== null) {
			return $this->content;
		}
		$this->content = new stdClass;
		$this->content->text = '<a href="'.$CFG->wwwroot.'/blocks/androgogic_training_history/index.php">'.get_string('training_history_search','block_androgogic_training_history').'</a><br>';
                $this->content->text .= '<a href="'.$CFG->wwwroot.'/blocks/androgogic_training_history/index.php?tab=cpd_report">'.get_string('cpd_report','block_androgogic_training_history').'</a><br>';
		return $this->content;

	} //end function get_content

	function cron(){
block_androgogic_training_history_cron();
	} //end function cron

} //end class block_androgogic_training_history

// End of blocks/androgogic_training_history/block_androgogic_training_history.php