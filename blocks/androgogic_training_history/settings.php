<?php

/** 
 * Androgogic Training History Block: Settings
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/



defined('MOODLE_INTERNAL') || die(); 



if ($ADMIN->fulltree) {
//cron hours
$settings->add(new admin_setting_configtext('androgogic_training_history_run_cron_hours', get_string('cron_hours', 'block_androgogic_training_history'),
               get_string('cron_hours_explanation', 'block_androgogic_training_history'), '', PARAM_RAW,50));
//workflow
$settings->add(new admin_setting_configcheckbox('androgogic_training_history_approval_workflow', get_string('approval_workflow', 'block_androgogic_training_history'),
               get_string('approval_workflow_explanation', 'block_androgogic_training_history'), '', PARAM_BOOL));
}




// End of blocks/androgogic_training_history/settings.php
