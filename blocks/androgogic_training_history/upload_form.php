<?php

/** 
 * Block Androgogic Training History Block: Upload form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     03/07/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/
require_once($CFG->libdir.'/formslib.php');

class block_androgogic_training_history_uploadsingle_form extends moodleform {
    function definition() {
        $mform = $this->_form;
        $instance = $this->_customdata;

        // visible elements
        $mform->addElement('filepicker', 'itemid', get_string('uploadafile'), null, null);
        $mform->addRule('itemid', get_string('uploadnofilefound'), 'required', null, 'client');

        // hidden params
        $mform->addElement('hidden', 'tab', $_REQUEST['tab']);
        $mform->addElement('hidden', 'object', $instance['object']);
        $mform->addElement('hidden', 'objectid', $instance['objectid']);
        $mform->addElement('hidden', 'fileid', $instance['fileid']);
        $mform->addElement('hidden', 'block_name', $instance['block_name']);
        $mform->addElement('hidden', 'table_name', $instance['table_name']);
        $mform->addElement('hidden', 'foreign_key_name', $instance['foreign_key_name']);

        // buttons
        $this->add_action_buttons(true, get_string('savechanges', 'admin'));
    }
}