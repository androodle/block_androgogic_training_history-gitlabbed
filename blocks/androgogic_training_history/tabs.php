<?php
/** 
 * Androgogic Training History Block: Tabs
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

// This file to be included so we can assume config.php has already been included.
if (empty($currenttab)) {
error('You cannot call this script in that way');
}
$tabs = array();
$row = array();
$row[] = new tabobject('training_history', $CFG->wwwroot.'/blocks/androgogic_training_history/index.php?tab=training_history_search', get_string('training_history_search','block_androgogic_training_history'));
$row[] = new tabobject('cpd_report', $CFG->wwwroot.'/blocks/androgogic_training_history/index.php?tab=cpd_report', get_string('cpd_report','block_androgogic_training_history'));

if(has_capability('block/androgogic_training_history:admin', $context)){
$row[] = new tabobject('activities', $CFG->wwwroot.'/blocks/androgogic_training_history/index.php?tab=activity_search', get_string('activity_search','block_androgogic_training_history'));
}
$tabs[] = $row;
// Print out the tabs and continue!
print_tabs($tabs, $currenttab);

// End of blocks/androgogic_training_history/tabs.php